/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getMetadataArgsStorage } from 'typeorm';
import { DictionaryEntity } from './features/eng-amh-dictionary/dictionary.entity';
import { DictionaryModule } from './features/eng-amh-dictionary/dictionary.module';
import { GeneratorModule } from './features/amh-txt-generator/generator.module';
import { AmhMorphologyModule } from './features/amh-morphology-dictionary/amh.morphology.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '127.0.0.1',
      username: 'postgres',
      password: 'Harry@2',
      database: 'text_generation_db',
      schema: 'text-generation',
      entities: getMetadataArgsStorage().tables.map((tbl) => tbl.target),
      synchronize: true,
    }),
    DictionaryModule,
    AmhMorphologyModule,
    GeneratorModule,
  ],
})
export class AppModule {}
