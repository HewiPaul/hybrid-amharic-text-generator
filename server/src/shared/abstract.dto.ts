/* eslint-disable prettier/prettier */
import { BaseEntity, PrimaryGeneratedColumn } from 'typeorm';

export abstract class AbstractEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment') id: number;
}
