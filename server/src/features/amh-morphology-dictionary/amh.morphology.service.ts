/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { getConnection } from 'typeorm';
import { AmhMorphologyEntity } from './amh.morphology.entity';
import { InitialWordDto } from './dto/initial.word.dto';
const { Translate } = require('@google-cloud/translate').v2;

// Creates a client
console.log(process.env.CREDENTIAL);
const credentials_ = {
  type: 'service_account',
  project_id: 'analog-sum-449',
  private_key_id: 'bffcc928a717022b31dd5925af1c7d2ad4413c43',
  private_key:
    '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDuIlSYynvPMIbD\nR7uTalLUkAVir4ClOfQWCA9jA2wb7mXdIxP+duEcZ1P7LOBycI55ZNSI87ZoLpjF\nAU91z6VfE5ev2cfWIE3oNIUawWCXh/FD6j9G50Rax9Nyns3qJd5VQeHa74Z3ubrA\nTXYRQHPRIzR3XOzqCS52c9y6D3T9amTndL4YHf+kCI3YARO3JHIDGE1n+b/8QO+6\nYrqqOSIj3VNzIlwjDqa90GygKruR2fkwhxUIwMQ30U83E2E/vgQZp7w0lZX8E2J2\nDUwT3SU3uMZ0x7GVCaqP+NXke+dTz//o2y/ukbaz0NL2HONWbZTK/cosjdR7zmnx\nbh88EF9HAgMBAAECggEAK6Oq8R0aN0vjRmIc49T2PPHvqrgS0TSfoHH1r+VuqGe1\nzyKghV9vvW7KGxuMPiyWT3TE5hwTb2Aqqt+kmihlW9kRUS6NypW88TK+ubp6YwqM\nki+ZTPsQsbow665ieehaqDEcnKyMbeBhcuA/JMY08zyKXt1upSztkNOGvaobfFzq\nhecP53+/sa7vK6Z5Wv4eGI2T5J5LQuuEPvTGbyDEkhmzhu0PJSxsl1ZTPHgLUXR9\n9O7nWdn608/3y6J5UElZ96c7y/YXh1DWvSKxoouPJtM3+myTFbXZwxhWdnFaCmnq\nRbrYxCCfwJuiPsSG0AaFsdvxpZKbTqHyw0U6/CVksQKBgQD4NlhfA9YJa3JIpLYr\nVjGOC4Chr9uT5PBIAT5oBCzuLkwh2I4+0aRjRSIi1ZMdf9ZfPI0GXLS2fVS4+OJg\nT5Cf8x9SO3B0rT9YOywKEPBA35Azoe6ZI/W39kAaPM1jNG/5pMyU0zN8LpZMhe1x\n4e8qiGwvWsNLRoP4s2yjkywacwKBgQD1mwlnl+73FfUFaWYrk6YA21XA5xfoc8Py\nkstzLaG3gBzRlcqYeXR1GWF0Z9TMNTs/X8XJFPoBlUCy0HP/5hoovnHUyAlNh29C\nP3QbEBKrbHFpvB0lLq5pWcXUiPLX8LzRXkBYifdcuhq0QhWpCMdyvEnTD24tnCz0\nWYDGMTfO3QKBgQDOrCmPC9mnREEHfaZ1lTJz7N3xQ0WY8pKj0e1NnNRwijo4IGHo\nqpSvV7whtyXk9THhYMCoIH8USG1IC2wq93Ubycs1pGpxrGPHKpXz18DEs6IVa+sI\ntml+ANvFPQVjaiEmg/MYpHcFGqFpHEbmgVKsXqV+FJvyTMfMSFHYLc4WQwKBgQCO\nYNoK2+EEDA6GnJtGF8ncJUqJEW6iqrmlHcQjeQiOwA300ckoqwAAAj6kpKyP1SwL\niRG3iBmGYWeoQX99kX8Ir559o5kBAMpWtUIxjhfEmJEGoq6ip3UkTY6JrvpYhI5x\nMRpMOW7Uqm5goQ69dwFoWg7R89gDSM4muNwWCOI5YQKBgAKhkeY8NBzeoQcmGmew\nWKjTeiS83zM3SqbUrqkdLeL+/3F8ecciFTFY9fZcwyY4ybEllPjt3E8LKYsCBZBU\nd6u0WzLDSjs+MrsKNyHZqhGe2as3iTbDysb0+xGOCN7odIwstS3AqfPKUMrr3UTD\nlVnmCc97KJs415tGCIMIaUdM\n-----END PRIVATE KEY-----\n',
  client_email: 'google-translate@analog-sum-449.iam.gserviceaccount.com',
  client_id: '105180273897555476016',
  auth_uri: 'https://accounts.google.com/o/oauth2/auth',
  token_uri: 'https://oauth2.googleapis.com/token',
  auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
  client_x509_cert_url:
    'https://www.googleapis.com/robot/v1/metadata/x509/google-translate%40analog-sum-449.iam.gserviceaccount.com',
};
const translate = new Translate({
  credentials: credentials_,
  projectId: credentials_.project_id,
});

@Injectable()
export class AmhMorphologyService extends TypeOrmCrudService<AmhMorphologyEntity> {
  constructor(@InjectRepository(AmhMorphologyEntity) repo) {
    super(repo);
  }

  private haletaw_ha = 'ሀ,ሁ,ሂ,ሃ,ሄ,ህ,ሆ';
  private le = 'ለ,ሉ,ሊ,ላ,ሌ,ል,ሎ';
  private hameru_ha = 'ሐ,ሑ,ሒ,ሓ,ሔ,ሕ,ሖ';
  private me = 'መ,ሙ,ሚ,ማ,ሜ,ም,ሞ';
  private se = 'ሠ,ሡ,ሢ,ሣ,ሤ,ሥ,ሦ';
  private re = 'ረ,ሩ,ሪ,ራ,ሬ,ር,ሮ';
  private nigusu_se = 'ሰ,ሱ,ሲ,ሳ,ሴ,ስ,ሶ';
  private she = 'ሸ,ሹ,ሺ,ሻ,ሼ,ሽ,ሾ';
  private qe = 'ቀ,ቁ,ቂ,ቃ,ቄ,ቅ,ቆ';
  private be = 'በ,ቡ,ቢ,ባ,ቤ,ብ,ቦ';
  private te = 'ተ,ቱ,ቲ,ታ,ቴ,ት,ቶ';
  private che = 'ቸ,ቹ,ቺ,ቻ,ቼ,ች,ቾ';
  private ha = 'ኀ,ኁ,ኂ,ኃ,ኄ,ኅ,ኆ';
  private ne = 'ነ,ኑ,ኒ,ና,ኔ,ን,ኖ';
  private gne = 'ኘ,ኙ,ኚ,ኛ,ኜ,ኝ,ኞ';
  private a = 'አ,ኡ,ኢ,ኣ,ኤ,እ,ኦ';
  private ke = 'ከ,ኩ,ኪ,ካ,ኬ,ክ,ኮ';
  private he = 'ኸ,ኹ,ኺ,ኻ,ኼ,ኽ,ኾ';
  private we = 'ወ,ዉ,ዊ,ዋ,ዌ,ው,ዎ';
  private aynu_a = 'ዐ,ዑ,ዒ,ዓ,ዔ,ዕ,ዖ';
  private ze = 'ዘ,ዙ,ዚ,ዛ,ዜ,ዝ,ዞ';
  private zhe = 'ዠ,ዡ,ዢ,ዣ,ዤ,ዥ,ዦ';
  private ye = 'የ,ዩ,ዪ,ያ,ዬ,ይ,ዮ';
  private de = 'ደ,ዱ,ዲ,ዳ,ዴ,ድ,ዶ';
  private je = 'ጀ,ጁ,ጂ,ጃ,ጄ,ጅ,ጆ';
  private ge = 'ገ,ጉ,ጊ,ጋ,ጌ,ግ,ጎ';
  private xe = 'ጠ,ጡ,ጢ,ጣ,ጤ,ጥ,ጦ';
  private chee = 'ጨ,ጩ,ጪ,ጫ,ጬ,ጭ,ጮ';
  private pee = 'ጰ,ጱ,ጲ,ጳ,ጴ,ጵ,ጶ';
  private tse = 'ጸ,ጹ,ጺ,ጻ,ጼ,ጽ,ጾ';
  private tsee = 'ፀ,ፁ,ፂ,ፃ,ፄ,ፅ,ፆ';
  private fe = 'ፈ,ፉ,ፊ,ፋ,ፌ,ፍ,ፎ';
  private pe = 'ፐ,ፑ,ፒ,ፓ,ፔ,ፕ,ፖ';
  private ve = 'ቨ,ቩ,ቪ,ቫ,ቬ,ቭ,ቮ';

  private amhAlphabets: string[] = [
    this.ve,
    this.pe,
    this.fe,
    this.tsee,
    this.tse,
    this.pee,
    this.chee,
    this.xe,
    this.ge,
    this.je,
    this.de,
    this.ye,
    this.zhe,
    this.ze,
    this.aynu_a,
    this.we,
    this.he,
    this.ke,
    this.gne,
    this.a,
    this.ne,
    this.ha,
    this.che,
    this.te,
    this.be,
    this.qe,
    this.she,
    this.nigusu_se,
    this.re,
    this.me,
    this.se,
    this.hameru_ha,
    this.le,
    this.haletaw_ha,
  ];

  async getAllAmhMorphology() {
    try {
      return await this.repo.find();
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async create(data: InitialWordDto) {
    try {
      const amhMorphology = new AmhMorphologyEntity();
      amhMorphology.eng_verb = data.engVerb;
      amhMorphology.amh_verb = data.initialWord;
      amhMorphology.first_person_male_singular = await this.get_first_person_male_singular_form(
        data.initialWord,
      );
      amhMorphology.first_person_male_singular_present = await this.get_first_person_male_singular_present_form(
        data.initialWord,
      );
      amhMorphology.first_person_male_singular_future = await this.get_first_person_male_singular_future_form(
        data.initialWord,
      );
      amhMorphology.first_person_male_singular_past = await this.get_first_person_male_singular_past_form(
        data.initialWord,
      );
      amhMorphology.first_person_female_singular_present = await this.get_first_person_female_singular_present_form(
        data.initialWord,
      );
      amhMorphology.first_person_female_singular_future = await this.get_first_person_female_singular_future_form(
        data.initialWord,
      );
      amhMorphology.first_person_female_singular_past = await this.get_first_person_female_singular_past_form(
        data.initialWord,
      );
      amhMorphology.second_person_male_singular = await this.get_second_person_male_singular_form(
        data.initialWord,
      );
      amhMorphology.second_person_male_singular_present = await this.get_second_person_male_singular_present_form(
        data.initialWord,
      );
      amhMorphology.second_person_male_singular_future = await this.get_second_person_male_singular_future_form(
        data.initialWord,
      );
      amhMorphology.second_person_male_singular_past = await this.get_second_person_male_singular_past_form(
        data.initialWord,
      );
      amhMorphology.second_person_female_singular = await this.get_second_person_female_singular_form(
        data.initialWord,
      );
      amhMorphology.second_person_female_singular_present = await this.get_second_person_female_singular_present_form(
        data.initialWord,
      );
      amhMorphology.second_person_female_singular_future = await this.get_second_person_female_singular_future_form(
        data.initialWord,
      );
      amhMorphology.second_person_female_singular_past = await this.get_second_person_female_singular_past_form(
        data.initialWord,
      );
      amhMorphology.third_person_male_singular = await this.get_third_person_male_singular_form(
        data.initialWord,
      );
      amhMorphology.third_person_male_singular_present = await this.get_third_person_male_singular_present_form(
        data.initialWord,
      );
      amhMorphology.third_person_male_singular_future = await this.get_third_person_male_singular_future_form(
        data.initialWord,
      );
      amhMorphology.third_person_male_singular_past = await this.get_third_person_male_singular_past_form(
        data.initialWord,
      );
      amhMorphology.first_person_female_singular = await this.get_first_person_female_singular_form(
        data.initialWord,
      );
      amhMorphology.first_person_female_singular_present = await this.get_first_person_female_singular_present_form(
        data.initialWord,
      );
      amhMorphology.second_person_female_singular_future = await this.get_second_person_female_singular_future_form(
        data.initialWord,
      );
      amhMorphology.third_person_female_singular = await this.get_third_person_female_singular_form(
        data.initialWord,
      );
      amhMorphology.third_person_female_singular_present = await this.get_third_person_female_singular_form_present(
        data.initialWord,
      );
      amhMorphology.third_person_female_singular_future = await this.get_third_person_female_singular_form_future(
        data.initialWord,
      );
      amhMorphology.third_person_female_singular_past = await this.get_third_person_female_singular_form_past(
        data.initialWord,
      );
      amhMorphology.first_person_plural = await this.get_first_person_plural_form(
        data.initialWord,
      );
      amhMorphology.first_person_plural_present = await this.get_first_person_plural_present_form(
        data.initialWord,
      );
      amhMorphology.first_person_plural_future = await this.get_first_person_plural_future_form(
        data.initialWord,
      );
      amhMorphology.first_person_plural_past = await this.get_first_person_plural_past_form(
        data.initialWord,
      );
      amhMorphology.second_person_plural = await this.get_second_person_plural_form(
        data.initialWord,
      );
      amhMorphology.second_person_plural_present = await this.get_second_person_plural_present_form(
        data.initialWord,
      );
      amhMorphology.second_person_plural_future = await this.get_second_person_plural_future_form(
        data.initialWord,
      );
      amhMorphology.second_person_plural_past = await this.get_second_person_plural_past_form(
        data.initialWord,
      );
      amhMorphology.third_person_plural = await this.get_third_person_plural_form(
        data.initialWord,
      );
      amhMorphology.third_person_plural_present = await this.get_third_person_plural_form_present(
        data.initialWord,
      );
      amhMorphology.third_person_plural_future = await this.get_third_person_plural_form_future(
        data.initialWord,
      );
      amhMorphology.third_person_plural_past = await this.get_third_person_plural_form_past(
        data.initialWord,
      );
      amhMorphology.do_by_male_on_male = await this.get_do_by_male_on_male_form(
        data.initialWord,
      );
      amhMorphology.first_person_singular_do_by_male_on_male = await this.get_first_person_do_by_male_on_male_form(
        data.initialWord,
      );
      amhMorphology.first_person_singular_by_male_on_female = await this.get_first_person_do_by_male_on_female_form(
        data.initialWord,
      );
      amhMorphology.first_person_singular_do_by_male_on_many = await this.get_first_person_do_by_male_on_many_form(
        data.initialWord,
      );
      amhMorphology.second_person_singular_do_by_male_on_male = await this.get_second_person_do_by_male_on_male_form(
        data.initialWord,
      );
      amhMorphology.second_person_singular_by_male_on_female = await this.get_second_person_do_by_male_on_female_form(
        data.initialWord,
      );
      amhMorphology.second_person_singular_by_female_on_female = await this.get_second_person_do_by_female_on_female_form(
        data.initialWord,
      );
      amhMorphology.second_person_singular_do_by_female_on_male = await this.get_second_person_do_by_female_on_male_form(
        data.initialWord,
      );
      amhMorphology.second_person_singular_do_by_male_on_many = await this.get_second_person_do_by_male_on_many_form(
        data.initialWord,
      );

      amhMorphology.do_by_female_on_female = await this.get_do_by_female_on_female_form(
        data.initialWord,
      );
      amhMorphology.do_by_female_on_male = await this.get_do_by_female_on_male_form(
        data.initialWord,
      );
      amhMorphology.do_by_female_on_many = await this.get_do_by_female_on_many_form(
        data.initialWord,
      );
      const morphology = this.repo.create(amhMorphology);
      await morphology.save();
      return { note: { ...morphology } };
    } catch (err) {
      throw new BadRequestException();
    }
  }

  async update(id: number, data: AmhMorphologyEntity) {
    await this.repo.update({ id }, data);
    return this.findOne(id);
  }

  async GetSingleAmhWordMorphology(word: string): Promise<AmhMorphologyEntity> {
    const queryBuilder = this.repo
      .createQueryBuilder('amh-morphology')
      .where('amh-morphology.rootWord = :rootWord', { rootWord: word });

    return queryBuilder.getOne();
  }

  async remove(_id) {
    try {
      await getConnection()
        .createQueryBuilder()
        .delete()
        .from('amh-morphology')
        .where({
          id: _id,
        })
        .execute();

      return true;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async get_first_person_male_singular_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ሁ';
        else {
          formedWord = word.slice(0, -1) + letter[5] + 'ሁ';
        }
      }
    });
    return formedWord;
  }

  async get_first_person_male_singular_present_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = 'እየ' + word + 'ሁ';
        else {
          formedWord = 'እየ' + word.slice(0, -1) + letter[5] + 'ሁ';
        }
      }
    });
    return formedWord;
  }

  async get_first_person_male_singular_future_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    console.log(lestFromLastCharacter);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'እ' + word.slice(0, -1) + letter[3] + 'ለሁ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'እ' + word.slice(0, 1) + leftLetter[5] + letter[3] + 'ለሁ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_male_singular_past_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ሁ';
        else {
          formedWord = word.slice(0, -1) + letter[5] + 'ሁ';
        }
      }
    });
    return formedWord;
  }

  async get_second_person_male_singular_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = word.slice(0, -1) + letter[5] + 'ክ';
      }
    });
    return formedWord;
  }

  async get_second_person_male_singular_present_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = 'እየ' + word.slice(0, -1) + letter[5] + 'ክ';
      }
    });
    return formedWord;
  }

  async get_second_person_male_singular_future_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'ት' + word.slice(0, -1) + letter[3] + 'ለክ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'ት' + word.slice(0, 1) + leftLetter[5] + letter[3] + 'ለክ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_male_singular_past_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[0] + 'ካል';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[0] + 'ካል';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_third_person_male_singular_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        formedWord = word;
      }
    });
    return formedWord;
  }

  async get_third_person_male_singular_present_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = 'እየ' + word.slice(0, -1) + letter[0];
      }
    });
    return formedWord;
  }

  async get_third_person_male_singular_future_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'ይ' + word.slice(0, -1) + letter[0] + 'ዋል';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'ይ' + word.slice(0, 1) + leftLetter[5] + letter[0] + 'ዋል';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_third_person_male_singular_past_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[1] + 'ኣል';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[1] + 'ኣል';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_female_singular_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ሁ';
        else {
          formedWord = word.slice(0, -1) + letter[5] + 'ሁ';
        }
      }
    });
    return formedWord;
  }

  async get_first_person_female_singular_present_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = 'እየ' + word + 'ሁ';
        else {
          formedWord = 'እየ' + word.slice(0, -1) + letter[5] + 'ሁ';
        }
      }
    });
    return formedWord;
  }

  async get_first_person_female_singular_future_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    console.log(lestFromLastCharacter);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[3] + 'ለዉ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[3] + 'ለዉ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_female_singular_past_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = word.slice(0, -1) + letter[5] + 'ኩ';
      }
    });
    return formedWord;
  }

  async get_second_person_female_singular_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = word.slice(0, -1) + letter[5] + 'ሽ';
      }
    });
    return formedWord;
  }

  async get_second_person_female_singular_present_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = 'እየ' + word.slice(0, -1) + letter[5] + 'ሽ';
      }
    });
    return formedWord;
  }

  async get_second_person_female_singular_future_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'ት' + word.slice(0, -1) + letter[2] + 'ኣለሽ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'ት' + word.slice(0, 1) + leftLetter[5] + letter[2] + 'ኣለሽ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_female_singular_past_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = word.slice(0, -1) + letter[0] + 'ሻል';
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[0] + 'ሻል';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[0] + 'ሻል';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_third_person_female_singular_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        formedWord = word + 'ች';
      }
    });
    return formedWord;
  }

  async get_third_person_female_singular_form_present(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = 'እየ' + word + 'ች';
      }
    });
    return formedWord;
  }

  async get_third_person_female_singular_form_future(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'ት' + word.slice(0, -1) + letter[3] + 'ለች';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'ት' + word.slice(0, 1) + leftLetter[5] + letter[3] + 'ለች';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_third_person_female_singular_form_past(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[3] + 'ለች';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[3] + 'ለች';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_plural_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = word.slice(0, -1) + letter[5] + 'ን';
      }
    });
    return formedWord;
  }

  async get_first_person_plural_present_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = 'እየ' + word.slice(0, -1) + letter[5] + 'ን';
      }
    });
    return formedWord;
  }

  async get_first_person_plural_future_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'እን' + word.slice(0, -1) + letter[3] + 'ለን';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'እን' + word.slice(0, 1) + leftLetter[5] + letter[3] + 'ለን';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_plural_past_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[0] + 'ናል';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[0] + 'ናል';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_plural_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = word.slice(0, -1) + letter[3] + 'ችሁ';
      }
    });
    return formedWord;
  }

  async get_second_person_plural_present_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = 'እየ' + word.slice(0, -1) + letter[3] + 'ችሁ';
      }
    });
    return formedWord;
  }

  async get_second_person_plural_future_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'ት' + word.slice(0, -1) + letter[3] + 'ላችሁ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'ት' + word.slice(0, 1) + leftLetter[5] + letter[3] + 'ላችሁ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_plural_past_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = word.slice(0, -1) + letter[3] + 'ቹአል';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[3] + 'ቹአል';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_third_person_plural_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = word.slice(0, -1) + letter[1];
      }
    });
    return formedWord;
  }

  async get_third_person_plural_form_present(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        formedWord = 'እየ' + word.slice(0, -1) + letter[1];
      }
    });
    return formedWord;
  }

  async get_third_person_plural_form_future(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = 'ይ' + word.slice(0, -1) + letter[3] + 'ሉ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord =
                'ይ' + word.slice(0, 1) + leftLetter[5] + letter[3] + 'ሉ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_third_person_plural_form_past(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[0] + 'ዋል';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[0] + 'ዋል';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_do_by_male_on_male_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[3] + 'ለዉ';
        else if (word.length >= 4) formedWord = word + 'ለዉ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[5] + letter[3] + 'ለዉ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_do_by_male_on_male_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[5] + 'ሁት';
        else if (word.length >= 4) formedWord = word + 'ሁት';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[0] + letter[5] + 'ሁት';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_do_by_male_on_female_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[5] + 'ኋት';
        else if (word.length >= 4) formedWord = word + 'ኋት';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[0] + letter[5] + 'ኋት';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_first_person_do_by_male_on_many_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2)
          formedWord = word.slice(0, -1) + letter[5] + 'ኋቸዉ';
        else if (word.length >= 4) formedWord = word + 'ኋቸዉ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[0] + letter[5] + 'ኋቸዉ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_do_by_male_on_male_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ዉ';
        else if (word.length >= 4) formedWord = word + 'ዉ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word + 'ዉ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_do_by_male_on_female_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[3] + 'ት';
        else if (word.length >= 4) formedWord = word + 'ት';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[0] + letter[3] + 'ት';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_do_by_female_on_female_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ቻት';
        else if (word.length >= 4) formedWord = word + 'ቻት';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word + 'ቻት';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_do_by_female_on_male_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ችዉ';
        else if (word.length >= 4) formedWord = word + 'ችዉ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word + 'ችዉ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_second_person_do_by_male_on_many_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    const lestFromLastCharacter = word.slice(1, -1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word.slice(0, -1) + letter[3] + 'ቸዉ';
        else if (word.length >= 4) formedWord = word + 'ቸዉ';
        else {
          this.amhAlphabets.forEach((leters) => {
            if (leters.includes(lestFromLastCharacter)) {
              const leftLetter = leters.split(',');
              formedWord = word.slice(0, 1) + leftLetter[0] + letter[3] + 'ቸዉ';
            }
          });
        }
      }
    });
    return formedWord;
  }

  async get_do_by_female_on_male_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ችዉ';
        else if (word.length >= 4) formedWord = word + 'ችዉ';
        else {
          formedWord = word + 'ችዉ';
        }
      }
    });
    return formedWord;
  }

  async get_do_by_female_on_female_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ቻት';
        else if (word.length >= 4) formedWord = word + 'ቻት';
        else {
          formedWord = word + 'ቻት';
        }
      }
    });
    return formedWord;
  }

  async get_do_by_female_on_many_form(word: string) {
    let formedWord = '';
    const lastCharacter = word.slice(-1);
    this.amhAlphabets.forEach((leters) => {
      if (leters.includes(lastCharacter)) {
        const letter = leters.split(',');
        if (word.length <= 2) formedWord = word + 'ቻቸዉ';
        else if (word.length >= 4) formedWord = word + 'ቻቸዉ';
        else {
          formedWord = word + 'ቻቸዉ';
        }
      }
    });
    return formedWord;
  }
}
