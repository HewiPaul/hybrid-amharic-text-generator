/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';

export class InitialWordDto {
  @ApiProperty()
  engVerb: string;
  @ApiProperty()
  initialWord: string;
}
