/* eslint-disable prettier/prettier */
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AmhMorphologyEntity } from './amh.morphology.entity';
import { AmhMorphologyService } from './amh.morphology.service';
import { InitialWordDto } from './dto/initial.word.dto';

@ApiTags('amhMorphology')
@Controller('amhMorphology')
export class AmharicMorphologyController {
  constructor(public amhMorphologyService: AmhMorphologyService) {}

  @Post('create')
  register(@Body() amhMorphology: InitialWordDto) {
    amhMorphology.initialWord = amhMorphology.initialWord.trimStart().trimEnd();
    return this.amhMorphologyService.create(amhMorphology);
  }

  @Get('getAll')
  async GetAll() {
    try {
      return await this.amhMorphologyService.getAllAmhMorphology();
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('findByWord/:word')
  findById(@Param('word') word: string) {
    return this.amhMorphologyService.GetSingleAmhWordMorphology(
      word.trimStart().trimEnd(),
    );
  }

  @Put('update/:id')
  update(
    @Param('id') id: number,
    @Body(ValidationPipe) data: AmhMorphologyEntity,
  ) {
    return this.amhMorphologyService.update(id, data);
  }

  @Delete('delete/:id')
  delete(@Param('id') id: number) {
    return this.amhMorphologyService.remove(id);
  }
}
