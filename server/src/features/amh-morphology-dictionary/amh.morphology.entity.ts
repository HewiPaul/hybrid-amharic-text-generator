/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';
import { Column, Entity } from 'typeorm';
import { AbstractEntity } from '../../shared/abstract.dto';

@Entity('amh-morphology')
export class AmhMorphologyEntity extends AbstractEntity {
  @ApiProperty()
  @Column('text', { nullable: true })
  eng_verb: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  amh_verb: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_male_singular: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_male_singular_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_male_singular_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_male_singular_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_female_singular: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_female_singular_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_female_singular_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_female_singular_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_plural: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_plural_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_plural_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_plural_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_male_singular: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_male_singular_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_male_singular_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_male_singular_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_female_singular: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_female_singular_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_female_singular_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_female_singular_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_plural: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_plural_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_plural_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_plural_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_male_singular: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_male_singular_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_male_singular_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_male_singular_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_female_singular: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_female_singular_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_female_singular_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_female_singular_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_plural: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_plural_present: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_plural_future: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  third_person_plural_past: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  do_by_male_on_male: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_singular_do_by_male_on_male: string; //ሰበርኩት

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_singular_by_male_on_female: string; //ሰበርኳት

  @ApiProperty()
  @Column('text', { nullable: true })
  first_person_singular_do_by_male_on_many: string; //ሰበርኳቸዉ

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_singular_do_by_male_on_male: string; //ሰበረዉ

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_singular_do_by_female_on_male: string; //ሰበረችዉ

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_singular_by_male_on_female: string; //ሰበራት

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_singular_by_female_on_female: string; //ሰበረቻት

  @ApiProperty()
  @Column('text', { nullable: true })
  second_person_singular_do_by_male_on_many: string; //ሰበራቸዉ

  @ApiProperty()
  @Column('text', { nullable: true })
  do_by_female_on_male: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  do_by_female_on_female: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  do_by_female_on_many: string;

  toJSON() {
    return classToPlain(this);
  }
}
