/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AmharicMorphologyController } from './amh.morphology.controller';
import { AmhMorphologyEntity } from './amh.morphology.entity';
import { AmhMorphologyService } from './amh.morphology.service';

@Module({
  imports: [TypeOrmModule.forFeature([AmhMorphologyEntity])],
  controllers: [AmharicMorphologyController],
  providers: [AmhMorphologyService],
})
export class AmhMorphologyModule {}
