/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { getConnection } from 'typeorm';
import { AmhMorphologyEntity } from '../amh-morphology-dictionary/amh.morphology.entity';
import { DictionaryEntity } from './dictionary.entity';

@Injectable()
export class DictionaryService extends TypeOrmCrudService<DictionaryEntity> {
  constructor(@InjectRepository(DictionaryEntity) repo) {
    super(repo);
  }

  async getAll(
    options: IPaginationOptions,
  ): Promise<Pagination<DictionaryEntity>> {
    const queryBuilder = this.repo.createQueryBuilder('eng-amh-dictionary');
    return paginate<DictionaryEntity>(queryBuilder, options);
  }

  async create(dictionary: DictionaryEntity) {
    try {
      const dictionary_ = await this.repo.create(dictionary);
      await dictionary_.save();
      return { dictionary: { dictionary_ } };
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async findById(id: number): Promise<DictionaryEntity> {
    const queryBuilder = this.repo
      .createQueryBuilder('eng-amh-dictionary')
      .where('eng-amh-dictionary.id = :Id', { Id: id });

    return queryBuilder.getOne();
  }

  async findVerbByUW(EngUW: string): Promise<AmhMorphologyEntity> {
    const queryBuilder = await getConnection()
      .getRepository(AmhMorphologyEntity)
      .createQueryBuilder()
      .select('amh-morphology')
      .from(AmhMorphologyEntity, 'amh-morphology')
      .where('amh-morphology.eng_verb = :engVerb', { engVerb: EngUW });

    return queryBuilder.getOne();
  }

  async findByEngUW(UW: string): Promise<DictionaryEntity> {
    const queryBuilder = this.repo
      .createQueryBuilder('eng-amh-dictionary')
      .where('LOWER(eng-amh-dictionary.EN) = LOWER(:EN)', { EN: UW });

    return queryBuilder.getOne();
  }

  async update(id: number, data: DictionaryEntity) {
    await this.repo.update({ id }, data);
    return this.findById(id);
  }

  async remove(_id) {
    try {
      await getConnection()
        .createQueryBuilder()
        .delete()
        .from('eng-amh-dictionary')
        .where({
          id: _id,
        })
        .execute();

      return true;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
}
