/* eslint-disable prettier/prettier */
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DictionaryEntity } from './dictionary.entity';
import { DictionaryService } from './dictionary.service';

@ApiTags('dictionary')
@Controller('dictionary')
export class DictionaryController {
  constructor(public dictionaryService: DictionaryService) {}

  @Post('create')
  async register(@Body(ValidationPipe) note: DictionaryEntity) {
    return await this.dictionaryService.create(note);
  }

  @Get('getAll')
  async GetAll(@Query('page') page = 1, @Query('limit') limit = 100) {
    try {
      //const users = await this.service.findAll();
      limit = limit > 100 ? 100 : limit;
      return await this.dictionaryService.getAll({
        page,
        limit,
        route: 'http://localhost:3000/Dictionary',
      });
      //return res.status(HttpStatus.OK).json(users);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('findById/:id')
  findById(@Param('id') id: number) {
    return this.dictionaryService.findById(id);
  }

  @Put('update/:id')
  update(
    @Param('id') id: number,
    @Body(ValidationPipe) data: DictionaryEntity,
  ) {
    return this.dictionaryService.update(id, data);
  }

  @Delete('delete/:id')
  delete(@Param('id') id: number) {
    return this.dictionaryService.remove(id);
  }
}
