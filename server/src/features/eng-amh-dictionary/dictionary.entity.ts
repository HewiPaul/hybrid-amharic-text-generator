/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';
import { Column, Entity } from 'typeorm';
import { AbstractEntity } from '../../shared/abstract.dto';

@Entity('eng-amh-dictionary')
export class DictionaryEntity extends AbstractEntity {
  @ApiProperty()
  @Column('text', { nullable: true })
  en: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  public am: string;

  toJSON() {
    return classToPlain(this);
  }
}
