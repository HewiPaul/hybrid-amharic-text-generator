/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
  BadRequestException,
  Body,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { GeneratorService } from './generator.service';
import { diskStorage } from 'multer';
import { editFileName } from './filename.decorator';
import { unlDto, unlExpressionDto } from './unlDTO';

@ApiTags('generator')
@Controller('generator')
export class GeneratorController {
  constructor(private readonly service: GeneratorService) {}

  @Post('upload_unl_file')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
    }),
  )
  async uploadFile(@UploadedFile() file) {
    try {
      const distinctNodes = [
        ...new Set(
          await (await this.service.parseFile(file.path)).map(
            (expression) => expression,
          ),
        ),
      ];
      const verbNodes = await distinctNodes.filter(
        (node) =>
          node.expression.includes('@entry') ||
          node.expression.includes('icl>do'),
      );

      //Verb Nodes
      console.log('Verb Nodes: ');
      console.log(verbNodes);

      verbNodes.forEach((vNode) => {
        const verbIndex = distinctNodes.indexOf(vNode);
        distinctNodes.splice(verbIndex, 1);

        //push the verb to the last order since amharic sentence rule is SOV
        vNode.isVerb = true;
        distinctNodes.push(vNode);
      });

      //Parsed UNL Expression
      console.log('Pared UNL Nodes: ');
      console.log(distinctNodes);

      const amhWords = await this.service.generateAmharicText(distinctNodes);

      const amhSentence = amhWords.slice(0, amhWords.length).join(' ');

      return { data: { text: amhSentence, nodes: distinctNodes } };
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  @Post('parse_unl_expression')
  async parse(@Body() unl: unlDto) {
    try {
      const distinctNodes = [
        ...new Set(
          await (await this.service.parseExpression(unl.unlExpression)).map(
            (expression) => expression,
          ),
        ),
      ];

      //remove verb nodes from the beginning
      const verbNodes = await distinctNodes.filter(
        (node) =>
          node.expression.includes('@entry') ||
          node.expression.includes('icl>do'),
      );

      //Verb Nodes
      console.log('Verb Nodes: ');
      console.log(verbNodes);

      verbNodes.forEach((vNode) => {
        const verbIndex = distinctNodes.indexOf(vNode);
        distinctNodes.splice(verbIndex, 1);

        //push the verb to the last order since amharic sentence rule is SOV
        vNode.isVerb = true;
        distinctNodes.push(vNode);
      });

      //Parsed UNL Expression
      console.log('Pared UNL Nodes: ');
      console.log(distinctNodes);

      const amhWords = await this.service.generateAmharicText(distinctNodes);

      const amhSentence = amhWords.slice(0, amhWords.length).join(' ');

      return { data: { text: amhSentence, nodes: distinctNodes } };
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  @Post('generateText')
  async generateText(@Body() unl: unlExpressionDto) {
    try {
      return {
        data: await this.service.generateParsedUNL(unl.parsedUnlExpression),
      };
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }
}
