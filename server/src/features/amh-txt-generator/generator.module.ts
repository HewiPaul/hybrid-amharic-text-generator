/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DictionaryEntity } from '../eng-amh-dictionary/dictionary.entity';
import { DictionaryModule } from '../eng-amh-dictionary/dictionary.module';
import { DictionaryService } from '../eng-amh-dictionary/dictionary.service';
import { GeneratorController } from './generator.controller';
import { GeneratorService } from './generator.service';

@Module({
  imports: [TypeOrmModule.forFeature([DictionaryEntity]), DictionaryModule],
  controllers: [GeneratorController],
  providers: [GeneratorService, DictionaryService],
})
export class GeneratorModule {}
