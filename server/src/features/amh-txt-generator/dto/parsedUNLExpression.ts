/* eslint-disable prettier/prettier */
export class ParsedUnlExpression {
  public nodeType: string;
  public expression: string;
  public isVerb: boolean;
}
