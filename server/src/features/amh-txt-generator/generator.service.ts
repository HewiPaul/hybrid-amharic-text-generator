/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Repository } from 'typeorm';
import { DictionaryEntity } from '../eng-amh-dictionary/dictionary.entity';
import { DictionaryService } from '../eng-amh-dictionary/dictionary.service';
import { ParsedUnlExpression } from './dto/parsedUNLExpression';
const { Translate } = require('@google-cloud/translate').v2;

// Creates a client
console.log(process.env.CREDENTIAL);
const credentials_ = {
  type: 'service_account',
  project_id: 'analog-sum-449',
  private_key_id: 'bffcc928a717022b31dd5925af1c7d2ad4413c43',
  private_key:
    '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDuIlSYynvPMIbD\nR7uTalLUkAVir4ClOfQWCA9jA2wb7mXdIxP+duEcZ1P7LOBycI55ZNSI87ZoLpjF\nAU91z6VfE5ev2cfWIE3oNIUawWCXh/FD6j9G50Rax9Nyns3qJd5VQeHa74Z3ubrA\nTXYRQHPRIzR3XOzqCS52c9y6D3T9amTndL4YHf+kCI3YARO3JHIDGE1n+b/8QO+6\nYrqqOSIj3VNzIlwjDqa90GygKruR2fkwhxUIwMQ30U83E2E/vgQZp7w0lZX8E2J2\nDUwT3SU3uMZ0x7GVCaqP+NXke+dTz//o2y/ukbaz0NL2HONWbZTK/cosjdR7zmnx\nbh88EF9HAgMBAAECggEAK6Oq8R0aN0vjRmIc49T2PPHvqrgS0TSfoHH1r+VuqGe1\nzyKghV9vvW7KGxuMPiyWT3TE5hwTb2Aqqt+kmihlW9kRUS6NypW88TK+ubp6YwqM\nki+ZTPsQsbow665ieehaqDEcnKyMbeBhcuA/JMY08zyKXt1upSztkNOGvaobfFzq\nhecP53+/sa7vK6Z5Wv4eGI2T5J5LQuuEPvTGbyDEkhmzhu0PJSxsl1ZTPHgLUXR9\n9O7nWdn608/3y6J5UElZ96c7y/YXh1DWvSKxoouPJtM3+myTFbXZwxhWdnFaCmnq\nRbrYxCCfwJuiPsSG0AaFsdvxpZKbTqHyw0U6/CVksQKBgQD4NlhfA9YJa3JIpLYr\nVjGOC4Chr9uT5PBIAT5oBCzuLkwh2I4+0aRjRSIi1ZMdf9ZfPI0GXLS2fVS4+OJg\nT5Cf8x9SO3B0rT9YOywKEPBA35Azoe6ZI/W39kAaPM1jNG/5pMyU0zN8LpZMhe1x\n4e8qiGwvWsNLRoP4s2yjkywacwKBgQD1mwlnl+73FfUFaWYrk6YA21XA5xfoc8Py\nkstzLaG3gBzRlcqYeXR1GWF0Z9TMNTs/X8XJFPoBlUCy0HP/5hoovnHUyAlNh29C\nP3QbEBKrbHFpvB0lLq5pWcXUiPLX8LzRXkBYifdcuhq0QhWpCMdyvEnTD24tnCz0\nWYDGMTfO3QKBgQDOrCmPC9mnREEHfaZ1lTJz7N3xQ0WY8pKj0e1NnNRwijo4IGHo\nqpSvV7whtyXk9THhYMCoIH8USG1IC2wq93Ubycs1pGpxrGPHKpXz18DEs6IVa+sI\ntml+ANvFPQVjaiEmg/MYpHcFGqFpHEbmgVKsXqV+FJvyTMfMSFHYLc4WQwKBgQCO\nYNoK2+EEDA6GnJtGF8ncJUqJEW6iqrmlHcQjeQiOwA300ckoqwAAAj6kpKyP1SwL\niRG3iBmGYWeoQX99kX8Ir559o5kBAMpWtUIxjhfEmJEGoq6ip3UkTY6JrvpYhI5x\nMRpMOW7Uqm5goQ69dwFoWg7R89gDSM4muNwWCOI5YQKBgAKhkeY8NBzeoQcmGmew\nWKjTeiS83zM3SqbUrqkdLeL+/3F8ecciFTFY9fZcwyY4ybEllPjt3E8LKYsCBZBU\nd6u0WzLDSjs+MrsKNyHZqhGe2as3iTbDysb0+xGOCN7odIwstS3AqfPKUMrr3UTD\nlVnmCc97KJs415tGCIMIaUdM\n-----END PRIVATE KEY-----\n',
  client_email: 'google-translate@analog-sum-449.iam.gserviceaccount.com',
  client_id: '105180273897555476016',
  auth_uri: 'https://accounts.google.com/o/oauth2/auth',
  token_uri: 'https://oauth2.googleapis.com/token',
  auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
  client_x509_cert_url:
    'https://www.googleapis.com/robot/v1/metadata/x509/google-translate%40analog-sum-449.iam.gserviceaccount.com',
};
const translate = new Translate({
  credentials: credentials_,
  projectId: credentials_.project_id,
});

@Injectable()
export class GeneratorService {
  constructor(
    @InjectRepository(DictionaryEntity)
    private dictionaryRepo: Repository<DictionaryEntity>,
    private readonly dictionaryService: DictionaryService,
  ) {}

  async parseFile(file: string) {
    try {
      const parsedExpressions: string[] = [];
      const content = await readFileSync(file, 'utf8');
      await this.validateUNLExpression(content);

      //remove opener {unl} and  end {/unl}
      let parsedString = content.replace('{unl}', '');
      parsedString = parsedString.replace('{UNL}', '');
      parsedString = parsedString.replace('{/unl}', '');
      parsedString = parsedString.replace('{/UNL}', '');

      const UNLChars = [...parsedString];

      let parentBracketCloser = 0;

      let indexController = 0;

      UNLChars.forEach((c, i) => {
        if (c === '(') {
          parentBracketCloser = parentBracketCloser + 1;
        } else if (c === ')') {
          if (parentBracketCloser > 0) {
            parentBracketCloser = parentBracketCloser - 1;
          }
          if (parentBracketCloser === 0) {
            const node = parsedString
              .substring(indexController, i + 1)
              .trim()
              .replace(':01', ''); // Some unl expressions contains agt:01...
            indexController = i + 1;
            parsedExpressions.push(node);
          }
        }
      });
      const orderedUNLExpression = await this.reorderGeneratedUNLExpression(
        parsedExpressions,
      );
      return this.generateParsedUNL(orderedUNLExpression);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async parseExpression(UNL: string) {
    try {
      const parsedExpressions: string[] = [];
      //remove opener {unl} and  end {/unl}
      let parsedString = UNL.replace('{unl}', '');
      parsedString = parsedString.replace('{UNL}', '');
      parsedString = parsedString.replace('{/unl}', '');
      parsedString = parsedString.replace('{/UNL}', '');

      const UNLChars = [...parsedString];

      let parentBracketCloser = 0;

      let indexController = 0;

      UNLChars.forEach((c, i) => {
        if (c === '(') {
          parentBracketCloser = parentBracketCloser + 1;
        } else if (c === ')') {
          if (parentBracketCloser > 0) {
            parentBracketCloser = parentBracketCloser - 1;
          }
          if (parentBracketCloser === 0) {
            const node = parsedString
              .substring(indexController, i + 1)
              .trim()
              .replace(':01', ''); // Some unl expressions contains agt:01...
            indexController = i + 1;
            parsedExpressions.push(node);
          }
        }
      });

      const orderedUNLExpression = await this.reorderGeneratedUNLExpression(
        parsedExpressions,
      );
      return this.generateParsedUNL(orderedUNLExpression);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async generateParsedUNL(
    UNLExpression: string[],
  ): Promise<ParsedUnlExpression[]> {
    const parsedUNLExpressions: ParsedUnlExpression[] = [];

    UNLExpression.forEach(async (expression) => {
      //Expression Parser
      if (expression.trimStart().trimEnd().startsWith('agt')) {
        const expressions = await (await this.agt_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'agt';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('aoj')) {
        const expressions = await (await this.aoj_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'aoj';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('obj')) {
        const expressions = await (await this.obj_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'obj';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('plc')) {
        const expressions = await (await this.plc_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'plc';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('man')) {
        console.log('hello');
        const expressions = await (await this.man_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'man';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('ins')) {
        const expressions = await (await this.ins_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'ins';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('tim')) {
        const expressions = await (await this.tim_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'tim';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('tmt')) {
        const expressions = await (await this.tmt_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'tmt';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('tmf')) {
        const expressions = await (await this.tmf_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'tmf';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('rsn')) {
        const expressions = await (await this.rsn_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'rsn';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('mod')) {
        const expressions = await (await this.mod_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'mod';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('qua')) {
        const expressions = await (await this.qua_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'qua';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('dur')) {
        const expressions = await (await this.dur_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'dur';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('plt')) {
        const expressions = await (await this.plt_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'plt';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('plf')) {
        const expressions = await (await this.plf_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'plf';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('pur')) {
        const expressions = await (await this.pur_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'pur';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('and')) {
        const expressions = await (await this.and_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'and';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('gol')) {
        const expressions = await (await this.gol_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'gol';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }

      if (expression.trimStart().trimEnd().startsWith('ben')) {
        const expressions = await (await this.ben_parser(expression)).split(
          ',',
        );
        expressions.forEach((expression) => {
          if (
            !parsedUNLExpressions.find((expression_) => {
              if (
                expression_.expression.trimStart().trimEnd().toLowerCase() ===
                expression.trimStart().trimEnd().toLowerCase()
              ) {
                return expression;
              }
            })
          ) {
            const parsedExpression = new ParsedUnlExpression();
            parsedExpression.nodeType = 'ben';
            parsedExpression.expression = expression.trimStart().trimEnd();
            parsedExpression.isVerb = false;

            parsedUNLExpressions.push(parsedExpression);
          }
        });
      }
    });
    return parsedUNLExpressions;
  }

  async generateAmharicText(
    GeneratedUNLExpression: ParsedUnlExpression[],
  ): Promise<any> {
    let selectedAmharicUWs: string[] = [];
    let modCounter = 0;
    let andCounter = 0;
    selectedAmharicUWs = await Promise.all(
      GeneratedUNLExpression.map(async (expression) => {
        let UW = '';
        if (expression.expression.indexOf('(') > -1) {
          UW = expression.expression.substr(
            0,
            expression.expression.indexOf('('),
          );
        } else if (expression.expression.indexOf('.') > -1) {
          UW = expression.expression.substr(
            0,
            expression.expression.indexOf('.'),
          );
        } else UW = expression.expression;

        if (!expression.isVerb) {
          //non verb UV's
          //check and node redundancy for mod
          if (expression.nodeType == 'mod') {
            const AM_UW = (await translate.translate(UW, 'am'))[0];
            if (modCounter === 0) {
              const AMH_UW = this.AddMorphologyOnUW(
                expression,
                AM_UW != null ? AM_UW.trimStart()?.trimEnd() : 'ስም',
              );
              modCounter = modCounter + 1;
              return AMH_UW;
            }
            return AM_UW;
          } //check and node redundancy for and /እና/
          else if (expression.nodeType == 'and') {
            const AM_UW = (await translate.translate(UW, 'am'))[0];
            if (andCounter === 0) {
              const AMH_UW = this.AddMorphologyOnUW(
                expression,
                AM_UW != null ? AM_UW.trimStart()?.trimEnd() : 'ስም',
              );
              andCounter = andCounter + 1;
              return AMH_UW;
            }
            return AM_UW;
          } else {
            //read amharic equivalent from google translate api
            const AMH_UW = (await translate.translate(UW, 'am'))[0];
            return this.AddMorphologyOnUW(
              expression,
              AMH_UW != null ? AMH_UW.trimStart()?.trimEnd() : 'ስም',
            );
          }
        } else {
          //verb
          return this.AddMorphologyOnVerbNode(
            GeneratedUNLExpression,
            expression,
            UW,
            //AMH_UW != null ? AMH_UW.trimStart()?.trimEnd() : 'ስም',
          );
        }
      }),
    );
    modCounter = 0;
    andCounter = 0;
    return selectedAmharicUWs;
  }

  async reorderGeneratedUNLExpression(
    GeneratedUNLExpression: string[],
  ): Promise<string[]> {
    //Priority matrix for reordering of UNL expression
    const OrderedExpression: string[] = [];
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('agt')) {
        OrderedExpression.push(expression);
      }
    });

    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('aoj')) {
        OrderedExpression.push(expression);
      }
    });

    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('and')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('gol')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('ben')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('mod')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('obj')) {
        OrderedExpression.push(expression);
      }
    });

    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('rsn')) {
        OrderedExpression.push(expression);
      }
    });

    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('pur')) {
        OrderedExpression.push(expression);
      }
    });

    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('qua')) {
        OrderedExpression.push(expression);
      }
    });

    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('tim')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('tmt')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('tmf')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('dur')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('plc')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('man')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('plf')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('plt')) {
        OrderedExpression.push(expression);
      }
    });
    GeneratedUNLExpression.forEach((expression) => {
      if (expression.startsWith('ins')) {
        OrderedExpression.push(expression);
      }
    });
    return OrderedExpression;
  }

  async agt_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('agt(', '')
        .replace('agt (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async aoj_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('aoj(', '')
        .replace('aoj (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async obj_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('obj(', '')
        .replace('obj (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async plc_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('plc(', '')
        .replace('plc (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async man_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('man(', '')
        .replace('man (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async ins_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('ins(', '')
        .replace('ins (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async tim_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('tim(', '')
        .replace('tim (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async tmt_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('tmt(', '')
        .replace('tmt (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async tmf_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('tmf(', '')
        .replace('tmf (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async rsn_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('rsn(', '')
        .replace('rsn (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async mod_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('mod(', '')
        .replace('mod (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async qua_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('qua(', '')
        .replace('qua (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async dur_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('dur(', '')
        .replace('dur (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async plt_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('plt(', '')
        .replace('plt (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async plf_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('plf(', '')
        .replace('plf (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async pur_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('pur(', '')
        .replace('pur (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async and_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('and(', '')
        .replace('and (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async gol_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('gol(', '')
        .replace('gol (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async ben_parser(node: string): Promise<string> {
    try {
      return node
        .trimStart()
        .trimEnd()
        .replace('ben(', '')
        .replace('ben (', '')
        .slice(0, -1);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async AddMorphologyOnUW(node: any, amhWord: string): Promise<string> {
    try {
      let morpholgizedWord = amhWord;

      if (node.nodeType == 'agt') {
        if (
          node.expression.includes('@respect') &&
          node.expression.includes('@def')
        ) {
          morpholgizedWord = amhWord + ' የሆኑት ';
        } else if (
          node.expression.includes('@male') &&
          node.expression.includes('@def')
        ) {
          morpholgizedWord = amhWord + ' የሆነው ';
        } else if (
          node.expression.includes('@female') &&
          node.expression.includes('@def')
        ) {
          morpholgizedWord = amhWord + ' የሆነችው ';
        }
      } else if (node.nodeType == 'rsn') {
        morpholgizedWord = 'በ' + amhWord + ' ምክንያት';
      } else if (node.nodeType == 'man') {
        morpholgizedWord = amhWord;
      } else if (node.nodeType == 'plc') {
        if (node.expression.includes('@def')) {
          morpholgizedWord = amhWord + ' ዉስጥ';
        }
      } else if (node.nodeType == 'ins') {
        morpholgizedWord = 'በ' + amhWord;
      } else if (node.nodeType == 'obj') {
        //morpholgizedWord = amhWord + 'ውን';
        morpholgizedWord = amhWord + 'ን';
        if (node.expression.includes('@indef')) {
          morpholgizedWord = amhWord;
        }
      } else if (node.nodeType == 'plf') {
        morpholgizedWord = 'ከ' + amhWord;
      } else if (node.nodeType == 'plt') {
        morpholgizedWord = 'ወደ ' + amhWord;
      } else if (node.nodeType == 'tmt') {
        morpholgizedWord = 'እስከ ' + amhWord + ' ድረስ';
      } else if (node.nodeType == 'tmf') {
        morpholgizedWord = 'ከ' + amhWord + ' ጀምሮ';
      } else if (node.nodeType == 'qua') {
        morpholgizedWord = 'በ' + amhWord;
      } else if (node.nodeType == 'pur') {
        morpholgizedWord = 'ለ' + amhWord;
      } else if (node.nodeType == 'and') {
        morpholgizedWord = amhWord + ' እና ';
      } else if (node.nodeType == 'gol') {
        morpholgizedWord = 'ለ' + amhWord;
      } else if (node.nodeType == 'ben') {
        morpholgizedWord = 'ለ' + amhWord;
      } else if (node.nodeType == 'mod') {
        if (node.expression.includes('@def')) {
          morpholgizedWord = 'የ' + amhWord + 'ን';
        } else if (node.expression.includes('@indef')) {
          morpholgizedWord = amhWord;
        }
      }

      return morpholgizedWord;
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async AddMorphologyOnVerbNode(
    GeneratedUNLExpression: ParsedUnlExpression[],
    node: any,
    uw: string,
  ): Promise<string> {
    try {
      let morpholgizedWord = '';
      let subject = '';
      let object = '';
      //Add morphology on verb

      //Identify Subject and Object Gender
      GeneratedUNLExpression.forEach(async (expression) => {
        if (
          (expression.nodeType == 'agt' || expression.nodeType == 'aoj') &&
          expression.expression.includes('I(')
        ) {
          subject = '@I';
        } else if (
          (expression.nodeType == 'agt' || expression.nodeType == 'aoj') &&
          expression.expression.trim().toLowerCase() == 'we'
        ) {
          subject = '@WE';
        } else if (
          (expression.nodeType == 'agt' || expression.nodeType == 'aoj') &&
          expression.expression.trim().toLowerCase() == 'you'
        ) {
          subject = '@YOU';
        } else if (
          expression.nodeType == 'agt' &&
          (expression.expression.includes('@male.@respect') ||
            expression.expression.includes('@female.@respect'))
        ) {
          subject = '@respect';
        } else if (
          expression.nodeType == 'agt' &&
          (expression.expression.includes('@male') ||
            expression.expression.toLowerCase().trim() == 'he')
        ) {
          subject = '@male';
        } else if (
          expression.nodeType == 'agt' &&
          (expression.expression.includes('@female') ||
            expression.expression.toLowerCase().trim() == 'she')
        ) {
          subject = '@female';
        } else if (
          (expression.nodeType == 'obj' ||
            expression.nodeType == 'mod' ||
            expression.nodeType == 'gol' ||
            expression.nodeType == 'ben') &&
          (expression.expression.includes('@male.@respect') ||
            expression.expression.includes('@female.@respect'))
        ) {
          object = '@respect';
        } else if (
          (expression.nodeType == 'obj' ||
            expression.nodeType == 'mod' ||
            expression.nodeType == 'gol' ||
            expression.nodeType == 'ben') &&
          (expression.expression.includes('@male') ||
            expression.expression.toLowerCase().trim() == 'he')
        ) {
          object = '@male';
        } else if (
          (expression.nodeType == 'obj' ||
            expression.nodeType == 'mod' ||
            expression.nodeType == 'gol' ||
            expression.nodeType == 'ben') &&
          (expression.expression.includes('@female') ||
            expression.expression.toLowerCase().trim() == 'she')
        ) {
          object = '@female';
        }
      });
      console.log('Subject and Object: ' + subject + ' ' + object);
      //read amharic equivalent from local else from google translate api
      if (subject == '' && object == '') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.amh_verb;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@I' && object == '') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.first_person_male_singular;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@I' && object == '@male') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.first_person_singular_do_by_male_on_male;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@I' && object == '@female') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.first_person_singular_by_male_on_female;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@WE' && object == '') {
        const AMH_UW_LOCAL =
          (await (await this.dictionaryService.findVerbByUW(uw))
            ?.first_person_plural_present) + ' ነዉ';
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@YOU' && object == '') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.second_person_male_singular_future;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@male' && object == '') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.third_person_male_singular;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@male' && object == '') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.third_person_male_singular;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@female' && object == '') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.third_person_female_singular;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@male' && object == '@male') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.second_person_singular_do_by_male_on_male;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@male' && object == '@female') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.second_person_singular_by_male_on_female;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@female' && object == '@male') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.second_person_singular_do_by_female_on_male;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@female' && object == '@female') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.second_person_singular_by_female_on_female;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else if (subject == '@respect' || object == '@respect') {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.third_person_plural;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      } else {
        const AMH_UW_LOCAL = await (
          await this.dictionaryService.findVerbByUW(uw)
        )?.amh_verb;
        const AMH_UW = AMH_UW_LOCAL ?? (await translate.translate(uw, 'am'))[0];
        morpholgizedWord = AMH_UW;
      }
      return morpholgizedWord;
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async validateUNLExpression(expression: string) {
    try {
      if (!expression.toLowerCase().trimStart().trimEnd().startsWith('{unl}'))
        throw new BadRequestException(
          'Invalid UNL Expression Opening Syntax Detected! hint: {unl}',
        );
      if (!expression.toLowerCase().trimStart().trimEnd().endsWith('{/unl}'))
        throw new BadRequestException(
          'Invalid UNL Expression Closing Syntax Detected! hint: {/unl}',
        );
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }
}
