/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';

export class unlDto {
  @ApiProperty()
  unlExpression: string;
}

export class unlExpressionDto {
  @ApiProperty()
  parsedUnlExpression: string[];
}
