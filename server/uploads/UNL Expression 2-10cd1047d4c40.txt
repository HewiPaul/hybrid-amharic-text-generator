{unl}
agt(kill(icl>do).@past.@entry, Peter(icl> person).@male)
obj(kill(icl>do).@past.@entry, Mary(icl> person).@female)
ins(kill(icl>do).@past.@entry, knife(aoj> thing).@indef)
tim(kill(icl>do).@past.@entry, yesterday(aoj> thing))
rsn(kill(icl>do).@past.@entry, Jhon(icl> Person).@male)
plc(kill(icl>do).@past.@entry, kitchen(icl>place).@def)
{/unl}