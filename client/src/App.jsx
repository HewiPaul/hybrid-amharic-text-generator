/* eslint-disable react/jsx-no-undef */
import "./App.css"
import React, { useState } from "react"
import {
  Layout,
  Button,
  Form,
  Upload,
  Input,
  notification,
} from "antd"
import { InboxOutlined } from "@ant-design/icons"
function App() {
  const [generatedText, setGeneratedText] = useState("")
  const [process, setGeneratedNodes] = useState([])
  const [expression, setExpression] = useState("")
  const { Header, Footer, Content } = Layout
  const { TextArea } = Input

  const props = {
    name: 'file',
    multiple: false,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        notification.success({
          message:`${info.file.name} file uploaded successfully.`});
          const reader = new FileReader();
          reader.onload = (e) => {
            setExpression(e.target.result);
          };
          reader.readAsText(info.file.originFileObj)
      } else if (status === 'error') {
        notification.error({
          message:`${info.file.name} file upload failed.`});
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };

  const onChange = (e) => {
    console.log("Change:", e.target.value)
    setExpression(e.target.value)
  }

  const [parsingForm] = Form.useForm()

  const onFinish = async (values) => {
    fetch("http://localhost:4000/generator/parse_unl_expression", {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        //   'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify({
        unlExpression: expression,
      }), // body data type must match "Content-Type" header
    })
      .then(async (response) => response.json())
      .then(async (response) => {
        console.log("Res", response)
        setGeneratedText(response?.data.text)
        setGeneratedNodes([...response?.data.nodes])
        console.log("node", response?.data.nodes)
        notification.success({
          message: "Amharic Text Generation successful!!!",
          onClick: () => {
            console.log("Notification Clicked!")
          },
        })
      })

    /*  await validateFields()
      .then((values) => {
     
      })
      .catch((err) => {
        notification.error({
          message: "Something went wrong! Please try again." + err.message,
          onClick: () => {
            console.log("Notification Clicked!")
          },
        })
      }) */
  }

  const onFinishFailed = (errorInfo) => {
    return notification.success({
      message: "Unable to submit form!",
      onClick: () => {
        console.log("Notification Clicked!")
      },
    })
  }

  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
  }
  const normFile = (e) => {
    console.log("Upload event:", e)
    if (Array.isArray(e)) {
      return e
    }
    return e && e.fileList
  }
  return (
    <Layout className="layout">
      <Header>
        <div className="logo" style={{ color: "white" }}>
          AMH Text Generator
        </div>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <div className="site-layout-content">
          <Form
            name="validate_other"
            {...formItemLayout}
            onFinish={onFinish}
            form={parsingForm}
            onFinishFailed={onFinishFailed}
            initialValues={{
              "input-number": 3,
              "checkbox-group": ["A", "B"],
              rate: 3.5,
            }}
          >
            <Form.Item label="UNL File">
              <Form.Item
                name="unlFile"
                valuePropName="fileList"
                getValueFromEvent={normFile}
                noStyle
              >
                <Upload.Dragger {...props}>
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">Click or drag file to this area to upload</p>
                  <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                    band files
                  </p>
                </Upload.Dragger>
              </Form.Item>
            </Form.Item>

            <Form.Item label="UNL Expression">
              <Form.Item name="expression" valuePropName="expression" noStyle>
                <TextArea rows={3} showCount onChange={onChange} value={expression}/>
              </Form.Item>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                span: 12,
                offset: 6,
              }}
            >
              <Button type="primary" htmlType="submit">
                Generate Text
              </Button>
            </Form.Item>

            <Form.Item label="Process">
              <Form.Item
                name="process"
                valuePropName="process"
                //getValueFromEvent={normExpression}
                noStyle
              >
                {process.length > 0 &&
                  process.map((v, i) => (
                    <li key={i}>
                      ${v.nodeType} | ${v.expression}{" "}
                    </li>
                  ))}
              </Form.Item>
            </Form.Item>

            <Form.Item label="Generated Text">
              <Form.Item
                name="generated"
                valuePropName="generated"
                //getValueFromEvent={normExpression}
                noStyle
              >
                <TextArea
                  style={{ color: "black", fontWeight: "bold", pointerEvents: "none" }}
                  showCount
                  maxLength={10000}
                  rows={3}
                  onChange={onChange}
                  value={generatedText}
                />
              </Form.Item>
            </Form.Item>
          </Form>
        </div>
      </Content>
      <Footer>
        <span style={{ fontWeight: "bold" }}>Powered By: © HewiPaul</span>
      </Footer>
    </Layout>
  )
}

export default App
